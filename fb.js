// !!!MODIFY HERE!!!
var APP_ID = 'APPID HERE';

// !!!MODIFY HERE!!!
var OBJECT_TYPE = 'OBJECT TYPE';

window.fbAsyncInit = function() {
  FB.init({
    
    appId      : APP_ID,
    xfbml      : true,
    version    : 'v2.5'
  });
};

FB.getLoginStatus(function(response) {
	//console.log("LOGIN", response);
	if (response.status == 'connected') {
		onLogin(response);
	} else {
		FB.login(function(response) {
			onLogin(response);
		}, {
			scope: 'user_friends, email, publish_actions'
		});
	}
});

function onLogin(response) {
	if (response.status == 'connected') {
		FB.api('/me?fields=first_name', function(data) {
			//console.log(data.first_name);
			USER_ID = data.id;
		});
	}
};

window.fbAsyncInit();

function get_friends_data() {
  FB.api('/', 'POST', {
	batch: [
			{ 
				name:"get_friend_levels",
				method: 'GET', relative_url: '/'+ APP_ID +'/scores',
				omit_response_on_success: false
				
			},
			{
				name:"get_pictures",
				method: 'GET',
				relative_url: '/picture?redirect=false&ids={result=get_friend_levels:$.data.*.user.id}',
				depends_on: "get_friend_levels" 
			},
		]
	}, function (response) {
		//console.log("PICTURES",response);
		
		var levels = JSON.parse(response[0].body);
		var pictures = JSON.parse(response[1].body);
		
		for (var i = 0; i < levels.data.length; i++) {
			user_friends_game_data[levels.data[i].user.id] = { name : levels.data[i].user.name, level: levels.data[i].score };
		}
		
		for (var uid in pictures) {
			user_friends_game_data[uid].picture_url = pictures[uid].data.url;
		}
		//console.log("FRIENDS DATA", user_friends_game_data);
	});
}

function update_level(level) {
  FB.api(
    '/me/scores',
    'POST',
    {
      score: level
    },
    function(response) {
      //console.log(response);
    }
  );
  
  FB.api(
    'me/objects/' + DATA_TYPE,
    'post',
    {
      'object': {
        'og:title': LEVEL_NAME,
        'og:type': DATA_TYPE,
        'fb:app_id': APP_ID,
        'app:category': level,					// !!!MODIFY HERE!!! -> APP_NAME:FIELD
        'app:score': SCORE,             // !!!MODIFY HERE!!! -> APP_NAME:FIELD
        'app:rate': RATE                // !!!MODIFY HERE!!! -> APP_NAME:FIELD
      }
    },
    function(response) {
      //console.log(response);
    }
  );
}

function myFacebookLogin() {
	FB.login(
		function(response){
			onLogin(response);
		},
		{
			scope: 'publish_actions, email, user_friends'
		}
	);
}

