# Phaser-Facebook

## Constants

|   Constant   | Value |
|:-------------|:-----|
| APP_ID      | Facebook APP Id |
| OBJECT_TYPE | Name of created data type |